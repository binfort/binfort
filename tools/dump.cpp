#include <binfort/binfort.h>
using namespace binfort;

#include <iostream>

static bool dump(const char* filename)
{
  FileReader reader;
  if( !reader.open(filename) ) {
    std::cerr << "Could not open " << filename << " for reading!" << std::endl;
    return false;
  }

  Elem* elem = Elem::load(&reader, true);
  if( !elem ) {
    std::cerr << "Could not load " << filename << "!" << std::endl;
    return false;
  }
  elem->dump("");
  delete elem;

  return true;
}

int main(int argc, char** argv)
{
  for( int i = 1; i < argc; ++i ) {
    if( !dump(argv[i]) ) {
      return 1;
    }
  }

  return 0;
}
