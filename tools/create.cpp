#include <binfort/binfort.h>
using namespace binfort;

#include <iostream>

int main(int argc, char** argv)
{
  U16 name = 0;
  Elem* elem = Elem::create(name++);
  elem->append(U8Attr::create(name++, static_cast<U8>(8)));
  elem->append(U16Attr::create(name++, static_cast<U16>(16)));
  elem->append(U32Attr::create(name++, 32));
  elem->append(U64Attr::create(name++, 64LL));
  elem->append(U8ListAttr::create(name++, "hello"));
  elem->append(U16ListAttr::create(name++));
  elem->append(U32ListAttr::create(name++));
  elem->append(U64ListAttr::create(name++));

  Elem* subElem = Elem::create(name++);
  elem->append(subElem);
  subElem->append(U8Attr::create(name++, static_cast<U8>(8)));
  subElem->append(U16Attr::create(name++, static_cast<U16>(16)));
  subElem->append(U32Attr::create(name++, 32));
  subElem->append(U64Attr::create(name++, 64LL));
  subElem->append(U8ListAttr::create(name++, "hello"));
  subElem->append(U16ListAttr::create(name++));
  subElem->append(U32ListAttr::create(name++));
  subElem->append(U64ListAttr::create(name++));

  FileWriter writer;
  if(!writer.open("binfort-testfile.bft") ) {
    std::cerr << "Could not create test file!" << std::endl;
  }

  if(!elem->write(&writer, true)) {
    std::cerr << "Could not write file!" << std::endl;
  }

  delete elem;

  return 0;
}
