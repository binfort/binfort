binfort {#mainpage}
=======

binfort is a C++ library implementing a hierarchical serialization format
somewhat similar to XML, but using a very compact binary representation.

The foremost design objective is file size.

In contrast to XML, binfort only has two node types: elements and attributes.
Elements serve as a container for attributes and sub-elements.

Also, the format uses integral instead of textual "names" for elements
and attributes.

A "name" is a 12-bit unsigned integer, allowing for up to 4096
distinguishable element and attribute names each.

Each element has an overhead of 4 bytes.
Each attribute has an overhead of 2 bytes (4 bytes for lists).

The tree layout looks as follows:

- Root element
    - Attribute #0
    - Attribute #1
    - ...
    - Sub-element #0
        - Attribute #0
        - ...
    - Sub-element #1
        - ...

Attributes
----------
A total of eight different attribute types are specified.

### Value attributes ###
- U8
    - Single byte (signed and unsianged chars, bools).
- U16
    - 16 bit word (signed and unsigned 16bit integers).
- U32
    - 32 bit word (signed and unsigned 32 bit integers, floats).
- U64
    - 64 bit word (signed and unsigned 64 bit integers, doubles).

All value types are unsigned integer types. It is left up to the
interpretation to specify the acual type (ie float, int) used in
a given attribute. The library provides typical conversion
functions for the individual word types.

### List attributes ###
- LIST_U8
    - list of bytes, can be used for byte strings.
- LIST_U16
    - list of 16 bit words.
- LIST_U32
    - list of 32 bit words.
- LIST_U64
    - list of 64 bit words.

String encoding
---------------
binfort handles all strings as binary byte strings and does
not concern itself with their encoding.
It is recommended to either specify the encoding used in your
application, or add an attribute to the tree that specifies
the encoding.

Endianess
---------
Data is written and read in little-endian format, for example a
U32 value 0x11223344 will represented as 0x44, 0x33, 0x22, 0x11
on disk.

Disk format
-----------
### Node headers ###
A node header is a 16 bit value, made up of the following bits:

- Bit 15: node type
    - 0: element
    - 1: attribute
- Bits 12-14: node subtype
    - for elements:
        - 000: start element
        - 001: end element
    - for attributes:
        - 000: U8
        - 001: U16
        - 010: U32
        - 011: U64
        - 100: LIST_U8
        - 101: LIST_U16
        - 110: LIST_U32
        - 111: LIST_U64
- Bits 0-11: node name

### Element ###
- Node header
    - Type: element
    - Subtype: start element
    - Name: element name
- Element contents
    - zero or more attributes, followed by
    - zero or more sub-elements
- Node header
    - Type: element
    - Subtype: end element
    - Name: node name (same as above)

### Attribute ###
- Node header
    - Type: attribute
    - Subtype: attribute type
    - Name: attribute name
- Attribute value

### Attribute values ###
For a value attribute, a word corresponding to the attribute's value type.

For a list attribute, a 16 bit word containing the number of elements N,
followed by N words corresponding to the attribute's value type.

### File layout ###
- Magic number - BFT1 (4 bytes)
- Root element

TODO
----
- Node removal
- Node reordering
- Tree traversal ("cursor")
- Memory I/O
- Type conversion
    - bool
    - char
    - signed types
    - float, double
    - strings (char*, std::string)

WHISHLIST
---------
- XML de-/serialization
