#include <binfort/cursor.h>
using namespace binfort;

Cursor::Cursor(Elem* elem)
  : mIndex(0)
{
  if( elem ) {
    mStack.push_back(elem);
  }
}

bool Cursor::isNull() const
{
  return mStack.empty();
}

Cursor Cursor::firstChild() const
{
  if( isNull() ) {
    return Cursor();
  }
  
  Elem* elem = getElem();
  if( elem->getElems().empty() ) {
    return Cursor();
  }

  Cursor cursor(*this);
  cursor.mIndex = 0;
  cursor.mStack.push_back(elem->getElems()[0]);
  return cursor;
}

Cursor Cursor::firstChild(const U16 name) const
{
  if( isNull() ) {
    return Cursor();
  }
  
  Elem* elem = getElem();
  for( std::size_t i = 0; i < elem->getElems().size(); ++i ) {
    Elem* subElem = elem->getElems()[i];
    if( subElem->getName() == name ) {
      Cursor cursor(*this);
      cursor.mIndex = i;
      cursor.mStack.push_back(subElem);
      return cursor;
    }
  }

  return Cursor();
}

Cursor Cursor::nextSibling() const
{
  if( isNull() ) {
    return Cursor();
  }

  Cursor cursor(*this);
  cursor.mStack.pop_back();
  if( cursor.isNull() ) {
    return Cursor();
  }
  Elem* parent = cursor.getElem();
  Elem* sibling = parent->getElemAt(mIndex + 1);
  if( !sibling ) {
    return Cursor();
  }
  else {
    cursor.mIndex = mIndex + 1;
    cursor.mStack.push_back(sibling);
    return cursor;
  }
}

Cursor Cursor::nextSibling(const U16 name) const
{
  if( isNull() ) {
    return Cursor();
  }

  Cursor cursor(*this);
  cursor.mStack.pop_back();
  if( cursor.isNull() ) {
    return Cursor();
  }
  Elem* parent = cursor.getElem();
  for( std::size_t i = mIndex + 1; i < parent->getElems().size(); ++i ) {
    Elem* sibling = parent->getElemAt(i);
    if( sibling->getName() == name ) {
      cursor.mIndex = i;
      cursor.mStack.push_back(sibling);
      return cursor;
    }
  }
  return Cursor();
}

Elem* Cursor::getElem() const
{
  return mStack.empty()
    ? 0
    : mStack.back();
}

Attr* Cursor::getAttr(const U16 name) const
{
  Elem* elem = getElem();
  return elem
    ? elem->getAttr(name)
    : 0;
}
