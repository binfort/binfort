#include <binfort/binfort.h>
using namespace binfort;

#include <cstring>

const U8 binfort::MAGIC[4] = { 'B', 'F', 'T', '1' };

bool binfort::isValidName(const U16 name)
{
  return name == (name & NODE_NAME_MASK);
}

bool Writer::write(const U16 value)
{
  return
       write(static_cast<U8>( (value >> 8) & 0xFF ))
    && write(static_cast<U8>( (value >> 0) & 0xFF ));
}

bool Writer::write(const U32 value)
{
  return
       write(static_cast<U8>( (value >> 24) & 0xFF ))
    && write(static_cast<U8>( (value >> 16) & 0xFF ))
    && write(static_cast<U8>( (value >>  8) & 0xFF ))
    && write(static_cast<U8>( (value >>  0) & 0xFF ));
}

bool Writer::write(const U64 value)
{
  return
       write(static_cast<U8>( (value >> 56) & 0xFFLL ))
    && write(static_cast<U8>( (value >> 48) & 0xFFLL ))
    && write(static_cast<U8>( (value >> 40) & 0xFFLL ))
    && write(static_cast<U8>( (value >> 32) & 0xFFLL ))
    && write(static_cast<U8>( (value >> 24) & 0xFFLL ))
    && write(static_cast<U8>( (value >> 16) & 0xFFLL ))
    && write(static_cast<U8>( (value >>  8) & 0xFFLL ))
    && write(static_cast<U8>( (value >>  0) & 0xFFLL ));
}

FileWriter::FileWriter()
  : mFile(0)
{
}

FileWriter::~FileWriter()
{
  close();
}

bool FileWriter::open(const char* filename)
{
  close();
  mFile = fopen(filename, "wb+");
  return mFile != 0;
}

void FileWriter::close()
{
  if( mFile ) {
    fclose(mFile);
    mFile = 0;
  }
}

bool FileWriter::write(const U8 value)
{
  if( !mFile ) {
    return false;
  }
  else {
    return fputc(value, mFile) == value;
  }
}

template<typename T>
static bool readBytes(Reader* reader, T& value)
{
  value = static_cast<T>(0);
  for( std::size_t i = 0; i < sizeof(T); ++i ) {
    U8 byte;
    if( !reader->read(byte) ) {
      return false;
    }
    value |= ( static_cast<T>(byte) << ((sizeof(T) - i - 1) * 8) );
  }
  return true;
}

bool Reader::read(U16& value)
{
  return readBytes(this, value);
}

bool Reader::read(U32& value)
{
  return readBytes(this, value);
}

bool Reader::read(U64& value)
{
  return readBytes(this, value);
}

FileReader::FileReader()
  : mFile(0)
{
}

FileReader::~FileReader()
{
  close();
}

bool FileReader::open(const char* filename)
{
  close();
  mFile = fopen(filename, "rb");
  return mFile != 0;
}

void FileReader::close()
{
  if( mFile ) {
    fclose(mFile);
    mFile = 0;
  }
}

bool FileReader::read(U8& value)
{
  if( !mFile ) {
    return false;
  }
  const int c = fgetc(mFile);
  if( c == EOF ) {
    return false;
  }
  else {
    value = static_cast<U8>(c);
    return true;
  }
}

static U16 startElem(const U16 name)
{
  return (ELEM << NODE_TYPE_SHIFT) | (ELEM_START << NODE_SUBTYPE_SHIFT) | name;
}

static U16 endElem(const U16 name)
{
  return (ELEM << NODE_TYPE_SHIFT) | (ELEM_END << NODE_SUBTYPE_SHIFT) | name;
}

static U16 startAttr(const AttrType type, const U16 name)
{
  return (ATTR << NODE_TYPE_SHIFT) | (type << NODE_SUBTYPE_SHIFT) | name;
}

static U16 nodeType(const U16 header)
{
  return (header & NODE_TYPE_MASK) >> NODE_TYPE_SHIFT;
}

static U16 nodeSubtype(const U16 header)
{
  return (header & NODE_SUBTYPE_MASK) >> NODE_SUBTYPE_SHIFT;
}

static U16 nodeName(const U16 header)
{
  return header & NODE_NAME_MASK;
}

Elem* Elem::create(const U16 name)
{
  if( !isValidName(name) ) {
    return 0;
  }
  else {
    return new Elem(name);
  }
}

Elem* Elem::load(Reader* reader, const bool readMagic)
{
  if( readMagic ) {
    for( int i = 0; i < 4; ++i ) {
      U8 byte;
      if( !reader->read(byte) ) {
	return 0;
      }
      else if( byte != MAGIC[i] ) {
	return 0;
      }
    }
  }

  U16 header;
  if( !reader->read(header) ) {
    return 0;
  }
  else {
    if( nodeType(header) != ELEM ) {
      return 0;
    }
    else if( nodeSubtype(header) != ELEM_START ) {
      return 0;
    }
    else {
      Elem* elem = create(nodeName(header));
      if( !elem ) {
	return 0;
      }
      else if( !elem->loadContent(reader) ) {
	delete elem;
	return 0;
      }
      else {
	return elem;
      }
    }
  }
}

Elem::Elem(const U16 name)
  : mName(name)
{
}

Elem::~Elem()
{
  for( std::size_t i = 0; i < mAttrs.size(); ++i ) {
    delete mAttrs[i];
  }
  for( std::size_t i = 0; i < mElems.size(); ++i ) {
    delete mElems[i];
  }
}

bool Elem::write(Writer* writer, const bool writeMagic) const
{
  if( writeMagic ) {
    if( ! ( writer->write(MAGIC[0])
	    && writer->write(MAGIC[1])
	    && writer->write(MAGIC[2])
	    && writer->write(MAGIC[3]) ) ) {
      return false;
    }
  }

  return writer->write( startElem(mName) )
    && writeAttrs(writer)
    && writeElems(writer)
    && writer->write( endElem(mName) );
}

void Elem::append(Attr* attr)
{
  mAttrs.push_back(attr);
}

Attr* Elem::getAttr(const U16 name) const
{
  for( std::vector<Attr*>::const_iterator it = mAttrs.begin();
       it != mAttrs.end();
       ++it ) {
    if( (*it)->getName() == name ) {
      return *it;
    }
  }
  return 0;
}

void Elem::append(Elem* elem)
{
  mElems.push_back(elem);
}

Elem* Elem::getElemAt(const size_t index) const
{
  return (index < mElems.size())
    ? mElems[index]
    : 0;
}

void Elem::dump(const std::string& indent) const
{
  std::cout << indent << "E: " << mName << std::endl;
  for( std::size_t i = 0; i < mAttrs.size(); ++i ) {
    mAttrs[i]->dump(indent + "  ");
  }
  for( std::size_t i = 0; i < mElems.size(); ++i ) {
    mElems[i]->dump(indent + "  ");
  }
}

const std::vector<Attr*>& Elem::getAttrs() const
{
  return mAttrs;
}

std::vector<Attr*>& Elem::getAttrs()
{
  return mAttrs;
}

const std::vector<Elem*>& Elem::getElems() const
{
  return mElems;
}

std::vector<Elem*>& Elem::getElems()
{
  return mElems;
}

bool Elem::writeAttrs(Writer* writer) const
{
  for( std::size_t i = 0; i < mAttrs.size(); ++i ) {
    if( !mAttrs[i]->write(writer) ) {
      return false;
    }
  }
  return true;
}

bool Elem::writeElems(Writer* writer) const
{
  for( std::size_t i = 0 ; i < mElems.size(); ++i ) {
    if( !mElems[i]->write(writer, false) ) {
      return false;
    }
  }
  return true;
}

bool Elem::loadContent(Reader* reader)
{
  for(;;) {
    U16 header;
    if( !reader->read(header) ) {
      return false;
    }
    else {
      if( nodeType(header) == ELEM ) {
	if( nodeSubtype(header) == ELEM_END ) {
	  return nodeName(header) == mName;
	}
	else if( nodeSubtype(header) == ELEM_START ) {
	  Elem* elem = create(nodeName(header));
	  if( !elem ) {
	    return false;
	  }
	  else if( !elem->loadContent(reader) ) {
	    delete elem;
	    return false;
	  }
	  else {
	    mElems.push_back(elem);
	  }
	}
      }
      else if( nodeType(header) == ATTR ) {
	Attr* attr = 0;
	switch( nodeSubtype(header) ) {
	case ATTR_U8:
	  attr = U8Attr::create(nodeName(header));
	  break;
	case ATTR_U16:
	  attr = U16Attr::create(nodeName(header));
	  break;
	case ATTR_U32:
	  attr = U32Attr::create(nodeName(header));
	  break;
	case ATTR_U64:
	  attr = U64Attr::create(nodeName(header));
	  break;
	case ATTR_LIST_U8:
	  attr = U8ListAttr::create(nodeName(header));
	  break;
	case ATTR_LIST_U16:
	  attr = U16ListAttr::create(nodeName(header));
	  break;
	case ATTR_LIST_U32:
	  attr = U32ListAttr::create(nodeName(header));
	  break;
	case ATTR_LIST_U64:
	  attr = U64ListAttr::create(nodeName(header));
	  break;
	}
	if( !attr ) {
	  return false;
	}
	else if( !attr->loadValue(reader) ) {
	  return false;
	}
	else {
	  mAttrs.push_back(attr);
	}
      }
      else {
	return false;
      }
    }
  }
}

Attr::Attr(const AttrType type, const U16 name)
  : mType(type)
  , mName(name)
{
}

bool Attr::write(Writer* writer) const
{
  return writer->write(startAttr(mType, mName))
    && writeValue(writer);
}

void Attr::dump(const std::string& indent) const
{
  std::cout << indent << "A: " << mType << " " << mName << std::endl;
  dumpValue(indent + "   ");
}

void U8Attr::setValue(const bool value)
{
  mValue = value ? 1 : 0;
}

void U8Attr::setValue(const S8 value)
{
  mValue = reinterpret_cast<const U8&>(value);
}

bool U8Attr::asBool() const
{
  return mValue != 0;
}

signed char U8Attr::asSigned() const
{
  return reinterpret_cast<const S8&>(mValue);
}

void U16Attr::setValue(const S16 value)
{
  mValue = reinterpret_cast<const U16&>(value);
}

S16 U16Attr::asSigned() const
{
  return reinterpret_cast<const S16&>(mValue);
}


void U32Attr::setValue(const S32 value)
{
  mValue = reinterpret_cast<const U32&>(value);
}

void U32Attr::setValue(const float value)
{
  mValue = reinterpret_cast<const U32&>(value);
}

S32 U32Attr::asSigned() const
{
  return reinterpret_cast<const S32&>(mValue);
}

float U32Attr::asFloat() const
{
  return reinterpret_cast<const float&>(mValue);
}

void U64Attr::setValue(const S64 value)
{
  mValue = reinterpret_cast<const U64&>(value);
}

void U64Attr::setValue(const double value)
{
  mValue = reinterpret_cast<const U64&>(value);
}

S64 U64Attr::asSigned() const
{
  return reinterpret_cast<const S64&>(mValue);
}

double U64Attr::asDouble() const
{
  return reinterpret_cast<const double&>(mValue);
}

void U8ListAttr::setValue(const std::string& value)
{
  mList.clear();
  mList.insert(mList.begin(), value.begin(), value.end());
}

void U8ListAttr::setValue(const char* value)
{
  setValue(value, strlen(value));
}

void U8ListAttr::setValue(const char* value, std::size_t size)
{
  mList.clear();
  mList.insert(mList.begin(), &value[0], &value[size]);
}

std::string U8ListAttr::asString() const
{
  std::string string;
  string.insert(string.begin(), mList.begin(), mList.end());
  return string;
}
