#ifndef BINFORT_CURSOR_H
#define BINFORT_CURSOR_H

#include <binfort/binfort.h>

#include <vector>

namespace binfort
{
  //! \brief Convenient iteration over element trees.
  class Cursor
  {
  public:
    //! \brief Constructs a new cursor pointing to the given element.
    //!
    //! Note that the cursor will not be able to look up elements above
    //! the "root" element given here (ie it will *not* be able to find
    //! siblings of `root`, even if `root` is not the actual root element
    //! of a tree and *has* sibling elements).
    Cursor(Elem* root = 0);

    //! \brief Checks whether the cursor points to an element.
    bool isNull() const;

    //! \brief Returns a new cursor pointing to the first sub-element.
    //!
    //! Returns a NULL cursor if there are no sub-elements.
    Cursor firstChild() const;

    //! \brief Returns a new cursor pointing to the first sub-element with the given name.
    //!
    //! Returns a NULL cursor if there is no such sub-element.
    Cursor firstChild(const U16 name) const;

    //! \brief Returns a new cursor pointing to the next sibling element of the element the cursor currently points to.
    //!
    //! Returns a NULL cursor if there is no such sibling element.
    Cursor nextSibling() const;

    //! \brief Returns a new cursor pointing to the next sibling element with the given name.
    //!
    //! Returns a NULL-Cursor if there is no such sibling element.
    Cursor nextSibling(const U16 name) const;

    //! \brief Returns the element that the cursor points to.
    Elem* getElem() const;

    //! \brief Returns the attribute with the given name from the element the cursor points to.
    //!
    //! Returns NULL if there is no such attribute.
    Attr* getAttr(const U16 name) const;

  private:
    std::size_t mIndex;
    std::vector<Elem*> mStack;
  };

} // ns binfort

#endif /* BINFORT_CURSOR_H */
