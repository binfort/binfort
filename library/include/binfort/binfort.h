#ifndef _BINFORT_H_
#define _BINFORT_H_

#include <binfort/config.h>

#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

#if defined(BINFORT_HAVE_STDINT_H)
#include <stdint.h>
#endif

// \FIXME: Pointers vs. references: Pointers are used for Reader/Writer, references are used for read(U8&) etc.
//         1) Reverse
//             References are used for Reader/Writer, since they are not modified
//             read(U8*) should be used, to signal to the caller that the value will be modified.
//         2) All references
//            Uniform
//            No null-pointers
//         3) All pointers
//            Uniform
//            Advantage from 1)
//
// \FIXME: Ambiguous overloads
//         Currently the convenience overloads (setValue(3.14)) cause a lot of inconvenience
//         because casting is required.
//         1) Rename convenience overloads
//            Breaks generic setValue() used in templates
//         2) Remove obvious conversions
//            setValue(bool), setValue(SXX)
//            May lead to the compiler complaining
namespace binfort
{
#if defined(HAVE_STDINT_H)
  typedef uint8_t U8;
  typedef uint16_t U16;
  typedef uint32_t U32;
  typedef uint64_t U64;
  typedef sint8_t S8;
  typedef sint16_t S16;
  typedef sint32_t S32;
  typedef sint64_t S64;
#else
  // FIXME: This assumes GCC conventions and >= 32 bit systems
  typedef unsigned char U8;
  typedef unsigned short U16;
  typedef unsigned int U32;
  typedef unsigned long long U64;
  typedef signed char S8;
  typedef signed short S16;
  typedef signed int S32;
  typedef signed long long S64;
#endif

  //! \brief Magic number used as file header.
  extern const U8 MAGIC[4];

  //! \brief Values for the type field of a node header.
  enum NodeType {
    ELEM = 0,
    ATTR = 1,
  };

  //! \brief Values for the subtype field of an element node header.
  enum ElemType {
    ELEM_START = 0,
    ELEM_END   = 1,
  };

  //! \brief Values for the subtype field of an attribute node header.
  enum AttrType {
    ATTR_U8       = 0,
    ATTR_U16      = 1,
    ATTR_U32      = 2,
    ATTR_U64      = 3,
    ATTR_LIST_U8  = 4,
    ATTR_LIST_U16 = 5,
    ATTR_LIST_U32 = 6,
    ATTR_LIST_U64 = 7,
  };

  enum {
    NODE_TYPE_SHIFT = 15,
    NODE_SUBTYPE_SHIFT = 12,
    // 1000 0000 0000 0000
    NODE_TYPE_MASK = 1 << NODE_TYPE_SHIFT,
    // 0111 0000 0000 0000
    NODE_SUBTYPE_MASK = 7 << NODE_SUBTYPE_SHIFT,
    // 0000 1111 1111 1111
    NODE_NAME_MASK = (1 << NODE_SUBTYPE_SHIFT) - 1,
  };

  //! \brief Checks whether a name value is within the valid bounds (ie < 2^16).
  bool isValidName(const U16 name);

  //! \brief Abstract I/O writer.
  //!
  //! Concrete sub-classes implement \ref write(const U8).
  class Writer
  {
  public:
    virtual ~Writer() {}

    //! \brief Writes a single byte.
    //!
    //! Must return false if the byte could not be written.
    virtual bool write(const U8 value) = 0;
    bool write(const U16 value);
    bool write(const U32 value);
    bool write(const U64 value);
  };

  //! \brief Concrete I/O writer implementation using
  //! standard C library stream I/O.
  class FileWriter : public Writer
  {
  public:
    FileWriter();
    ~FileWriter();

    //! \brief Opens a file for writing.
    //!
    //! Returns false if the file could not be opened.
    bool open(const char* filename);

    //! \brief Explicitly closes the internal file handle.
    //!
    //! The file handle is also closed implicitly when the
    //! object is deleted.
    void close();

    bool write(const U8 value);

  private:
    FileWriter(const FileWriter&);
    FileWriter& operator=(const FileWriter&);
    FILE* mFile;
  };

  //! \brief Abstract I/O reader.
  //!
  //! Concrete sub-classes implement \ref read(U8&).
  class Reader
  {
  public:
    virtual ~Reader() {}

    //! \brief Reads a single byte
    //!
    //! Expected to read a single byte and store it in 'value'.
    //!
    //! Must return false if the byte could not be read.
    virtual bool read(U8& value) = 0;
    bool read(U16& value);
    bool read(U32& value);
    bool read(U64& value);
  };

  //! \brief Concrete I/O reader implementation using
  //! standard C library stream I/O.
  class FileReader : public Reader
  {
  public:
    FileReader();
    ~FileReader();

    //! \brief Opens a file for reading.
    //!
    //! Returns false if the file could not be opened.
    bool open(const char* filename);

    //! \brief Explicitly closes the internal file handle.
    //!
    //! The internal file handle is also closed implicitly
    //! when the object is deleted.
    void close();

    bool read(U8& value);

  private:
    FileReader(const FileReader&);
    FileReader& operator=(const FileReader&);
    FILE* mFile;
  };

  class Attr;

  //! \brief Represents element nodes in the tree.
  //!
  //! Each element contains a name, a list of attributes
  //! and a list of sub-elements.
  class Elem
  {
  public:
    //! \brief Creates a new element.
    //!
    //! \return 0, if the element could not be created due to an invalid name.
    static Elem* create(const U16 name);

    // \FIXME: Use separate method instead of parameter
    //! \brief De-serializes an element from a reader.
    //!
    //! \param readMagic This should be set to "true" if the start of the
    //!     file is read (the root element). In that case, the MAGIC number
    //!     will be read and checked before the root element is read.
    //! \return 0, if the element could not be de-serialized.
    static Elem* load(Reader* reader, const bool readMagic = true);

    ~Elem();

    U16 getName() const
    {
      return mName;
    }

    // \FIXME Use separate method instead of parameter
    //! \brief Serializes the element.
    //!
    //! \param writeMagic This should be set to "true" (the default)
    //!    if the element is the root element, in which case
    //!    the MAGIC number will be written before the element.
    bool write(Writer* writer, const bool writeMagic = true) const;

    //! \brief Appends an attribute to the element.
    //!
    //! The element takes ownership of the attribute pointer.
    void append(Attr* attr);

    Attr* getAttr(const U16 name) const;

    //! \brief Appends a sub-element to the element.
    //!
    //! The element takes ownership of the sub-element pointer.
    void append(Elem* elem);

    Elem* getElemAt(const std::size_t index) const;

    //! \brief Dumps a text representation of the element and it's
    //! contents to stdout.
    void dump(const std::string& indent) const;

    //! \brief const-getter for the element's attribute list.
    const std::vector<Attr*>& getAttrs() const;
    //! \brief Getter for the element's attribute list.
    std::vector<Attr*>& getAttrs();

    //! \biref const-getter for the element's sub-element list.
    const std::vector<Elem*>& getElems() const;
    //! \biref Getter for the element's sub-element list.
    std::vector<Elem*>& getElems();

  private:
    Elem(const U16 name);
    Elem(const Elem&);
    Elem& operator=(const Elem&);
    bool writeAttrs(Writer* writer) const;
    bool writeElems(Writer* writer) const;
    bool loadContent(Reader* reader);
    U16 mName;
    std::vector<Attr*> mAttrs;
    std::vector<Elem*> mElems;
  };

  //! \brief Abstract base class for attribute nodes.
  //!
  //! Each element contains a name, a type and
  //! a value that depends on the type.
  //! Sub-classes are used to implement different
  //! attribute types.
  class Attr
  {
  public:
    virtual ~Attr() {}

    //! \brief Getter for the attribute type.
    AttrType getType() const
    {
      return mType;
    }

    //! \brief Getter for the attribute name.
    U16 getName() const
    {
      return mName;
    }

    //! \brief Downcast to a specific attribute type.
    //!
    //! Can be used in environments where dynamic_cast is
    //! not available.
    template<class Type>
    Type* castAs()
    {
      if( Type::Type() == getType() ) {
	return static_cast<Type*>(this);
      }
      else {
	return 0;
      }
    }

  protected:
    friend class Elem;
    Attr(const AttrType type, const U16 name);
    bool write(Writer* writer) const;
    void dump(const std::string& indent) const;
    virtual bool writeValue(Writer* writer) const = 0;
    virtual bool loadValue(Reader* reader) = 0;
    virtual void dumpValue(const std::string& indent) const = 0;

  private:
    Attr(const Attr&);
    Attr& operator=(const Attr&);
    AttrType mType;
    U16 mName;
  };

  //! \brief Internal helper class for implementing value attributes.
  template<class Value, class Derived>
  class ValueAttr : public Attr
  {
  public:
    //! \brief Factory method for value attributes.
    //!
    //! Returns the new attribute, or 0 if the name
    //! was invalid.
    static Derived* create(const U16 name)
    {
      if( isValidName(name) ) {
	return new Derived(name);
      }
      else {
	return 0;
      }
    }

    //! \brief Factory method for value attributes.
    //!
    //! Works like \ref create(const U16), but also
    //! passes a value to initialize the attribute with.
    template<class Type>
    static Derived* create(const U16 name, const Type value)
    {
      Derived* attr = create(name);
      if( !attr ) {
	return 0;
      }
      else {
	attr->setValue(value);
	return attr;
      }
    }

    //! \brief Value getter.
    Value getValue() const
    {
      return mValue;
    }

    //! \brief Value setter.
    void setValue(const Value value)
    {
      mValue = value;
    }

  protected:
    ValueAttr(const U16 name)
      : Attr(Derived::Type(), name)
    {
    }

    virtual bool writeValue(Writer* writer) const
    {
      return writer->write(mValue);
    }

    virtual bool loadValue(Reader* reader)
    {
      return reader->read(mValue);
    }

    virtual void dumpValue(const std::string& indent) const
    {
      std::cout << indent << mValue << std::endl;
    }

    Value mValue;
  };

  //! \brief Attribute containing a single byte.
  class U8Attr : public ValueAttr<U8, U8Attr>
  {
  public:
    static AttrType Type() { return ATTR_U8; }

    using ValueAttr<U8, U8Attr>::setValue;

    //! \brief Sets a boolean value.
    //!
    //! The byte representation is 0 or 1.
    void setValue(const bool value);

    //! \brief Sets a signed 8 bit value.
    void setValue(const S8 value);

    //! \brief Returns the value interpreted as a boolean.
    bool asBool() const;

    //! \brief Returns the value interpreted as a signed 8 bit word.
    S8 asSigned() const;

  protected:
    friend class ValueAttr<U8, U8Attr>;
    U8Attr(const U16 name)
      : ValueAttr<U8, U8Attr>(name)
    {
    }

    virtual void dumpValue(const std::string& indent) const
    {
      std::cout << indent << static_cast<int>(getValue()) << std::endl;
    }
  };

  //! \brief Attribute containing a single 16 bit word.
  class U16Attr : public ValueAttr<U16, U16Attr>
  {
  public:
    static AttrType Type() { return ATTR_U16; }

    using ValueAttr<U16, U16Attr>::setValue;

    //! \brief Sets a signed 32 bit value.
    void setValue(const S16 value);

    //! \brief Returns the value interpreted as a signed 16 bit word.
    S16 asSigned() const;

  protected:
    friend class ValueAttr<U16, U16Attr>;
    U16Attr(const U16 name)
      : ValueAttr<U16, U16Attr>(name)
    {
    }
  };

  //! \brief Attribute containing a single 32 bit word.
  class U32Attr : public ValueAttr<U32, U32Attr>
  {
  public:
    static AttrType Type() { return ATTR_U32; }

    using ValueAttr<U32, U32Attr>::setValue;

    //! \brief Sets a signed 32 bit value.
    void setValue(const S32 value);

    //! \brief Sets a float value.
    //!
    //! The internal 32 bit value is set to the float's binary representation.
    void setValue(const float value);

    //! \brief Returns the value interpreted as signed 32 bit word.
    S32 asSigned() const;

    //! \brief Returns the value interpreted as a float.
    //!
    //! The internal 32 bit value is interpreted as a float's binary representation.
    float asFloat() const;

  protected:
    friend class ValueAttr<U32, U32Attr>;
    U32Attr(const U16 name)
      : ValueAttr<U32, U32Attr>(name)
    {
    }
  };

  //! \brief Attribute containing a single 64 bit word.
  class U64Attr : public ValueAttr<U64, U64Attr>
  {
  public:
    static AttrType Type() { return ATTR_U64; }

    using ValueAttr<U64, U64Attr>::setValue;

    //! \brief Sets a signed 64 bit value.
    void setValue(const S64 value);

    //! \brief Sets a double value.
    //!
    //! The internal 64 bit value is set to the double's binary representation.
    void setValue(const double value);

    //! \brief Retunrn the value interpreted as a signed 64 bit word.
    S64 asSigned() const;

    //! \brief Returns the value interpreted as a double.
    //!
    //! The internal 64 bit value is interpreted as a double's binary representation.
    double asDouble() const;

  protected:
    friend class ValueAttr<U64, U64Attr>;
    U64Attr(const U16 name)
      : ValueAttr<U64, U64Attr>(name)
    {
    }
  };

  //! \brief Internal helper class for implementing list attributes.
  template<class Value, class Derived>
  class ListAttr : public Attr
  {
  public:
    //! \brief Factory method for list attributes.
    //!
    //! Returns the new attribute, or 0 if the name
    //! was invalid.
    static Derived* create(const U16 name)
    {
      if( isValidName(name) ) {
	return new Derived(name);
      }
      else {
	return 0;
      }
    }

    // \FIXME This should take vectors/iterators
    template<class Type>
    static Derived* create(const U16 name, const Type value)
    {
      Derived* attr = create(name);
      if( !attr ) {
	return 0;
      }
      else {
	attr->setValue(value);
	return attr;
      }
    }

    //! \brief Returns the length of the list.
    std::size_t getSize() const
    {
      return mList.size();
    }

    //! \brief Getter for the list.
    std::vector<Value>& getList()
    {
      return mList;
    }

    //! \brief const-getter for the list.
    const std::vector<Value>& getList() const
    {
      return mList;
    }

  protected:
    ListAttr(const U16 name)
      : Attr(Derived::Type(), name)
    {
    }

    virtual bool writeValue(Writer* writer) const
    {
      const std::size_t size = getSize();
      if( size >= (1 << 16) ) {
	return false;
      }
      else if( !writer->write(static_cast<U16>(size)) ) {
	return false;
      }
      else {
	for( std::size_t i = 0; i < size; ++i ) {
	  if( !writer->write(static_cast<Value>(mList[i])) ) {
	    return false;
	  }
	}
	return true;
      }
    }

    virtual bool loadValue(Reader* reader)
    {
      U16 size;
      if( !reader->read(size) ) {
	return false;
      }
      else {
	mList.clear();
	mList.reserve(size);
	for( U16 i = 0; i < size; ++i ) {
	  Value item;
	  if( !reader->read(item) ) {
	    return false;
	  }
	  else {
	    mList.push_back(item);
	  }
	}
	return true;
      }
    }

    virtual void dumpValue(const std::string& indent) const
    {
      std::cout << indent << "[" << getSize() << "]" << std::endl;
    }

    std::vector<Value> mList;
  };

  //! \brief Attribute containing a list of bytes.
  class U8ListAttr : public ListAttr<U8, U8ListAttr>
  {
  public:
    static AttrType Type() { return ATTR_LIST_U8; }

    //! \brief Sets the list from a string.
    void setValue(const std::string& value);

    //! \brief Sets the list from a 0-terminated string.
    void setValue(const char* value);

    //! \brief Sets the list from a char array.
    void setValue(const char* value, std::size_t size);

    //! \brief Returns the list interpreted as a string.
    std::string asString() const;

  protected:
    friend class ListAttr<U8, U8ListAttr>;
    U8ListAttr(const U16 name)
      : ListAttr<U8, U8ListAttr>(name)
    {
    }
  };

  //! \brief Attribute containing a list of 16 bit words.
  class U16ListAttr : public ListAttr<U16, U16ListAttr>
  {
  public:
    static AttrType Type() { return ATTR_LIST_U16; }

  protected:
    friend class ListAttr<U16, U16ListAttr>;
    U16ListAttr(const U16 name)
      : ListAttr<U16, U16ListAttr>(name)
    {
    }
  };

  //! \brief Attribute containing a list of 32 bit words.
  class U32ListAttr : public ListAttr<U32, U32ListAttr>
  {
  public:
    static AttrType Type() { return ATTR_LIST_U32; }

  protected:
    friend class ListAttr<U32, U32ListAttr>;
    U32ListAttr(const U16 name)
      : ListAttr<U32, U32ListAttr>(name)
    {
    }
  };

  //! \brief Attribute containing a list of 64 bit words.
  class U64ListAttr : public ListAttr<U64, U64ListAttr>
  {
  public:
    static AttrType Type() { return ATTR_LIST_U64; }

  protected:
    friend class ListAttr<U64, U64ListAttr>;
    U64ListAttr(const U16 name)
      : ListAttr<U64, U64ListAttr>(name)
    {
    }
  };

} // ns binfort

#endif /* _BINFORT_H_ */
