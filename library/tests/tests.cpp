#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>

#include <memory>

#include <binfort/binfort.h>
#include <binfort/cursor.h>
using namespace binfort;

#include <unistd.h>

static const char* FILENAME = "binfort-tests-filename.bft";

class Fixture : public CppUnit::TestFixture
{
public:
  void tearDown()
  {
    unlink(FILENAME);
  }

  void testInvalidAttrNameIsRejected()
  {
    std::auto_ptr<U8Attr> attr(U8Attr::create(1 << 12));
    CPPUNIT_ASSERT(!attr.get());
  }

  void testValidAttrNameIsAccepted()
  {
    std::auto_ptr<U8Attr> attr(U8Attr::create(1 << 11));
    CPPUNIT_ASSERT(attr.get());
  }

  void testBoolAttributeIsPreserved()
  {
    std::auto_ptr<U8Attr> attr(U8Attr::create(0));
    attr->setValue(true);
    CPPUNIT_ASSERT(attr->getValue());
    attr->setValue(false);
    CPPUNIT_ASSERT(!attr->getValue());
  }

  void testCharAttributeIsPreserved()
  {
    std::auto_ptr<U8Attr> attr(U8Attr::create(0));
    const signed char c = -1;
    attr->setValue(c);
    CPPUNIT_ASSERT_EQUAL(attr->asSigned(), c);
  }

  void testFloatAttributeIsPreserved()
  {
    std::auto_ptr<U32Attr> attr(U32Attr::create(0));
    const float f = 3.14;
    attr->setValue(f);
    CPPUNIT_ASSERT_EQUAL(attr->asFloat(), f);
  }

  void testDoubleAttributeIsPreserved()
  {
    std::auto_ptr<U64Attr> attr(U64Attr::create(0));
    const double d = 3.14;
    attr->setValue(d);
    CPPUNIT_ASSERT_EQUAL(attr->asDouble(), d);
  }

  void testStringAttributeIsPreserved()
  {
    std::auto_ptr<U8ListAttr> attr(U8ListAttr::create(0));
    const std::string s = "i am a test string";
    attr->setValue(s);
    CPPUNIT_ASSERT_EQUAL(attr->asString(), s);
  }

  void testCharStarAttributeIsPreserved()
  {
    std::auto_ptr<U8ListAttr> attr(U8ListAttr::create(0));
    const std::string s = "i am a test string";
    attr->setValue(s.c_str());
    CPPUNIT_ASSERT_EQUAL(attr->asString(), s);
  }

  void testCharArrayAttributeIsPreserved()
  {
    std::auto_ptr<U8ListAttr> attr(U8ListAttr::create(0));
    const std::string s = "i am a test string";
    attr->setValue(s.c_str(), s.size());
    CPPUNIT_ASSERT_EQUAL(attr->asString(), s);
  }

  void testAttributesArePreservedAcrossIO()
  {
    {
      U16 name = 0;
      std::auto_ptr<Elem> elem(Elem::create(name++));
      elem->append(U8Attr::create(name++, false));
      elem->append(U32Attr::create(name++, 3.14f));
      elem->append(U64Attr::create(name++, 3.14));
      elem->append(U8ListAttr::create(name++, "i am a test string"));

      FileWriter writer;
      CPPUNIT_ASSERT(writer.open(FILENAME));
      CPPUNIT_ASSERT(elem->write(&writer, true));
    }

    {
      FileReader reader;
      CPPUNIT_ASSERT(reader.open(FILENAME));
      std::auto_ptr<Elem> elem(Elem::load(&reader, true));
      CPPUNIT_ASSERT(elem.get());
      CPPUNIT_ASSERT(elem->getAttrs().size() == 4);
      CPPUNIT_ASSERT(dynamic_cast<U8Attr*>(elem->getAttrs()[0]));
      CPPUNIT_ASSERT_EQUAL(dynamic_cast<U8Attr*>(elem->getAttrs()[0])->asBool(), false);
      CPPUNIT_ASSERT(dynamic_cast<U32Attr*>(elem->getAttrs()[1]));
      CPPUNIT_ASSERT_EQUAL(dynamic_cast<U32Attr*>(elem->getAttrs()[1])->asFloat(), 3.14f);
      CPPUNIT_ASSERT(dynamic_cast<U64Attr*>(elem->getAttrs()[2]));
      CPPUNIT_ASSERT_EQUAL(dynamic_cast<U64Attr*>(elem->getAttrs()[2])->asDouble(), 3.14);
      CPPUNIT_ASSERT(dynamic_cast<U8ListAttr*>(elem->getAttrs()[3]));
      CPPUNIT_ASSERT_EQUAL(dynamic_cast<U8ListAttr*>(elem->getAttrs()[3])->asString(), std::string("i am a test string"));
    }
  }

  CPPUNIT_TEST_SUITE(Fixture);
  CPPUNIT_TEST(testInvalidAttrNameIsRejected);
  CPPUNIT_TEST(testValidAttrNameIsAccepted);
  CPPUNIT_TEST(testBoolAttributeIsPreserved);
  CPPUNIT_TEST(testCharAttributeIsPreserved);
  CPPUNIT_TEST(testFloatAttributeIsPreserved);
  CPPUNIT_TEST(testDoubleAttributeIsPreserved);
  CPPUNIT_TEST(testStringAttributeIsPreserved);
  CPPUNIT_TEST(testCharStarAttributeIsPreserved);
  CPPUNIT_TEST(testCharArrayAttributeIsPreserved);
  CPPUNIT_TEST(testAttributesArePreservedAcrossIO);
  CPPUNIT_TEST_SUITE_END();
};


class CursorFixture : public CppUnit::TestFixture
{
public:
  Elem* makeSingleton()
  {
    return Elem::create(0);
  }

  Elem* makeTree()
  {
    Elem* elem = Elem::create(0);
    Elem* subElem = Elem::create(1);
    subElem->append(U8Attr::create(0, static_cast<U8>(0)));
    elem->append(subElem);
    subElem = Elem::create(1);
    subElem->append(U8Attr::create(0, static_cast<U8>(1)));
    elem->append(subElem);
    return elem;
  }

  void testNull()
  {
    Cursor cursor(0);
    CPPUNIT_ASSERT(cursor.isNull());
  }

  void testNotNull()
  {
    std::auto_ptr<Elem> elem(makeSingleton());
    Cursor cursor(elem.get());
    CPPUNIT_ASSERT(!cursor.isNull());
  }

  void testNonExistingChildIsNull()
  {
    std::auto_ptr<Elem> elem(makeSingleton());
    Cursor cursor(elem.get());
    CPPUNIT_ASSERT(cursor.firstChild().isNull());
  }

  void testExistingChildIsNotNull()
  {
    std::auto_ptr<Elem> elem(makeTree());
    Cursor cursor(elem.get());
    CPPUNIT_ASSERT(!cursor.firstChild().isNull());
  }

  void testSiblingIsFound()
  {
    std::auto_ptr<Elem> elem(makeTree());
    Cursor cursor(elem.get());
    cursor = cursor.firstChild(1);
    CPPUNIT_ASSERT(!cursor.isNull());
    CPPUNIT_ASSERT(cursor.getElem()->getName() == 1);
    U8Attr* attr = dynamic_cast<U8Attr*>(cursor.getAttr(0));
    CPPUNIT_ASSERT(attr);
    CPPUNIT_ASSERT(attr->getValue() == 0);
    cursor = cursor.nextSibling(1);
    CPPUNIT_ASSERT(!cursor.isNull());
    CPPUNIT_ASSERT(cursor.getElem()->getName() == 1);
    attr = dynamic_cast<U8Attr*>(cursor.getAttr(0));
    CPPUNIT_ASSERT(attr);
    CPPUNIT_ASSERT(attr->getValue() == 1);
  }

  CPPUNIT_TEST_SUITE(CursorFixture);
  CPPUNIT_TEST(testNull);
  CPPUNIT_TEST(testNotNull);
  CPPUNIT_TEST(testNonExistingChildIsNull);
  CPPUNIT_TEST(testExistingChildIsNotNull);
  CPPUNIT_TEST(testSiblingIsFound);
  CPPUNIT_TEST_SUITE_END();
};

int main()
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(Fixture::suite());
  runner.addTest(CursorFixture::suite());
  return runner.run() ? 0 : 1;
}
